#!/bin/bash

### Introduction text for the user
echo "
This script is for building an
- Angular 4 project in a
- Test Driven Development environment with
- debug configured using the
- Visual Studio Code IDE.

** IMPORTANT: Make sure you have the following installed and up to date:
- NodeJS
- AngularCLI
- Visual Studio Code
- - VS Code Wallaby Plugin
- - Debugger for Chrome

This is a contribution from Polyphasic Developers
www.PolyphasicDevs.com

by
Brian Phillips
"

### Get input about project name from the user
echo "Enter the name of your project:"
read projectName

### Setup project name variable to be used in rest of script
ng new $projectName
cd ${projectName}/


## Creating Wallaby configuration files
touch wallaby.conf.js
cat > wallaby.conf.js << EOL
var wallabyWebpack = require('wallaby-webpack');
var path = require('path');

var compilerOptions = Object.assign(
  require('./tsconfig.json').compilerOptions,
  require('./src/tsconfig.spec.json').compilerOptions);

module.exports = function (wallaby) {

  var webpackPostprocessor = wallabyWebpack({
    entryPatterns: [
      'src/wallabyTest.js',
      'src/**/*spec.js'
    ],

    module: {
      loaders: [
        {test: /\.css$/, loader: 'raw-loader'},
        {test: /\.html$/, loader: 'raw-loader'},
        {test: /\.ts$/, loader: '@ngtools/webpack', include: /node_modules/, query: {tsConfigPath: 'tsconfig.json'}},
        {test: /\.js$/, loader: 'angular2-template-loader', exclude: /node_modules/},
        {test: /\.json$/, loader: 'json-loader'},
        {test: /\.styl$/, loaders: ['raw-loader', 'stylus-loader']},
        {test: /\.less$/, loaders: ['raw-loader', 'less-loader']},
        {test: /\.scss$|\.sass$/, loaders: ['raw-loader', 'sass-loader']},
        {test: /\.(jpg|png)$/, loader: 'url-loader?limit=128000'}
      ]
    },

    resolve: {
      extensions: ['.js', '.ts'],
      modules: [
        path.join(wallaby.projectCacheDir, 'src/app'),
        path.join(wallaby.projectCacheDir, 'src'),
        'node_modules'
      ]
    },
    node: {
      fs: 'empty',
      net: 'empty',
      tls: 'empty',
      dns: 'empty'
    }
  });

  return {
    files: [
      {pattern: 'src/**/*.ts', load: false},
      {pattern: 'src/**/*.d.ts', ignore: true},
      {pattern: 'src/**/*.css', load: false},
      {pattern: 'src/**/*.less', load: false},
      {pattern: 'src/**/*.scss', load: false},
      {pattern: 'src/**/*.sass', load: false},
      {pattern: 'src/**/*.styl', load: false},
      {pattern: 'src/**/*.html', load: false},
      {pattern: 'src/**/*.json', load: false},
      {pattern: 'src/**/*spec.ts', ignore: true}
    ],

    tests: [
      {pattern: 'src/**/*spec.ts', load: false}
    ],

    testFramework: 'jasmine',

    compilers: {
      '**/*.ts': wallaby.compilers.typeScript(compilerOptions)
    },

    middleware: function (app, express) {
      var path = require('path');
      app.use('/favicon.ico', express.static(path.join(__dirname, 'src/favicon.ico')));
      app.use('/assets', express.static(path.join(__dirname, 'src/assets')));
    },

    env: {
      kind: 'electron'
    },

    postprocessor: webpackPostprocessor,

    setup: function () {
      window.__moduleBundler.loadTests();
    },

    debug: true
  };
};

EOL

touch src/wallabyTest.ts
cat > src/wallabyTest.ts << EOL
import './polyfills';

import 'zone.js/dist/long-stack-trace-zone';
import 'zone.js/dist/proxy.js';
import 'zone.js/dist/sync-test';
import 'zone.js/dist/jasmine-patch';
import 'zone.js/dist/async-test';
import 'zone.js/dist/fake-async-test';

import { getTestBed } from '@angular/core/testing';
import {
  BrowserDynamicTestingModule,
  platformBrowserDynamicTesting
} from '@angular/platform-browser-dynamic/testing';

getTestBed().initTestEnvironment(
  BrowserDynamicTestingModule,
  platformBrowserDynamicTesting()
);

EOL


### Creating the Debugger configuration file
mkdir .vscode
touch .vscode/launch.json
cat > .vscode/launch.json << EOL
{
  // Use IntelliSense to learn about possible Node.js debug attributes.
  // Hover to view descriptions of existing attributes.
  // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
  "version": "0.2.0",
  "configurations": [
    {
      "name": "Launch Client",
      "type": "chrome",
      "request": "launch",
      "url": "http://localhost:4200/",
      "port": 9223,
      "sourceMaps": true,
      "diagnosticLogging": true,
      "webRoot": "\${workspaceRoot}",
      "runtimeArgs": [
          "--disable-session-crashed-bubble",
          "--disable-infobars"
      ],
      "userDataDir": "\${workspaceRoot}/.vscode/chrome"
  }
  ]
}

EOL


### Creating the Tasks configuration file
touch .vscode/tasks.json
cat > .vscode/tasks.json << EOL
{
    // See https://go.microsoft.com/fwlink/?LinkId=733558
    // for the documentation about the tasks.json format
    "version": "2.0.0",
    "tasks": [
      {
        "taskName": "ngServe",
        "type": "shell",
        "reveal": "always",
        "panel": "dedicated",
        "command": "ng serve --open",
        "group": {
          "kind": "build",
          "isDefault": true
        },
        "problemMatcher": []
      },
      {
        "taskName": "ngTest",
        "type": "shell",
        "reveal": "always",
        "panel": "dedicated",
        "command": "ng test --code-coverage",
        "group": {
          "kind": "build",
          "isDefault": true
        },
        "problemMatcher": []
      }
    ]
  }
EOL


### Installing packages for Wallaby
npm install wallaby-webpack angular2-template-loader electron --save-dev


### Run Visual Studio Code
code .

